//
// This file contains the answer to the brick wall problem (1.3)
// by: Jonathan Ma
//

#include <iostream>
#include <string>
#include <vector>
#include "brick_wall.h"

using namespace std;

//For some reason, I can't use to_string on MinGW. So I wrote my own function:

string intToString(int n)
{
    string line;

    while (n > 0)
    {
        line.insert(0, 1, n%10 + '0');
        n = n/10;
    }
    return line;
}

//Helper function for getting the number of digits in a number.
int numDigits(int number)
{
    int digits = 0;
    if (number < 0)
        digits = 1;
    while (number) {
        number = number / 10;
        digits++;
    }
    return digits;
}

//This helper function returns the factorial for calculating combinations.
int factorial(int n)
{
    int product = 1;
    for (int i = 1; i <= n; i++)
        product *= i;
    return product;
}

//This gets an entry based on the individual cell calculations
int getCombinationEntry(int top, int bottom)
{
    return factorial(top) / (factorial(bottom) * factorial(top - bottom));
}

//Gets an entire list for a given row in Pascal's triangle, based on the formula for calculating each element.
vector<int> getLine(int lineNumber)
{
    vector<int> line;
    for (int i = 0; i <= lineNumber; i++) {
        line.push_back(getCombinationEntry(lineNumber, i));
    }
    return line;
}

int main()
{
    //Gets input from the user
    int rows;
    cout << "Please enter the number of rows you'd like to print: ";
    cin >> rows;

    //Will hold the triangle in a data structure
    vector<vector<int>> list;

    //Calculates each row
    for (int i = 0; i <= rows; i++) {
        list.push_back(getLine(i));
    }

    int biggest_brick = getCombinationEntry(rows - 1, (rows - 1) / 2); //Calculates biggest cell's arithmetic value
    int brick_size = numDigits(biggest_brick) + 2; //Calculates the ideal size for brick
    string blank_brick; //Blank bricks needed for lower rows
    string brick; //Will hold the string for each brick
    int digit_size; //Holds the number of digits for each cell
    int digit_padding_size; //Holds the padding size for cell
    int number_of_blank_bricks; //Holds the number of blank bricks to draw
    string odd_padding; //Holds the size of beginning padding for odd rows.

    //Bricks must be odd sized (as per instructions in homework) to create aesthetic balance
    if (brick_size % 2 == 0)
        brick_size++;

    //Makes the odd padding
    for (int i = 0; i < (brick_size + 1) / 2; i++) {
        odd_padding += " ";
    }

    int row_number = 1; //holds the current row number
    int row_index = 0; //Indexes the row for each vector<int> in vector<vector<int>>

    //Constructs blank bricks to pad earlier rows
    for (int i = 0; i < brick_size; i++) {
        blank_brick += " ";
    }

    //Indexes by biggest rows to get padding for each row
    for (int i = rows - 1; i >= 0; i--) {
        //If the # of rows is even, need to pad by different number
        if (rows % 2 == 0)
            number_of_blank_bricks = (rows - row_number) / 2;
        else
            number_of_blank_bricks = (rows - row_number + 1) / 2;

        //Pads odd indices and even indices differently
        if (row_number % 2 == 1) {
            cout << odd_padding;
            for (int j = 0; j < number_of_blank_bricks; j++) {
                cout << blank_brick;
            }
        } else {
            for (int j = 0; j < number_of_blank_bricks; j++) {
                cout << blank_brick;
            }
        }

        //Increases the row number
        row_number++;

        //Holds an iterator to loop through elements in each vector<int>
        vector<int>::iterator vec_iterator;

        //Iterates through each element
        for (vec_iterator = list.at(row_index).begin(); vec_iterator != list.at(row_index).end(); vec_iterator++) {
            //Gets digit size to create brick
            digit_size = numDigits(*vec_iterator);
            digit_padding_size = (brick_size - digit_size) / 2;

            //Pads beginning of brick
            for (int i = 0; i < digit_padding_size; i++)
                brick += " ";

            //Adds digit to brick
            brick += intToString(*vec_iterator);

            //Adds extra padding depending on parity of number of digits
            if (digit_size % 2 == 0)
                for (int i = 0; i < digit_padding_size + 1; i++)
                    brick += " ";
            else
                for (int i = 0; i < digit_padding_size; i++)
                    brick += " ";

            //Prints brick
            cout << brick;

            //Resets brick
            brick = "";
        }

        //Increases the row index for the vector<vector<int>
        row_index++;

        //Next line
        cout << endl;
    }
}