//
// Created by majonathany on 1/7/17.
//

#include "tester.h"
#include "frame1.h"
#include "frame2.h"
#include <iostream>
#include <string>

using namespace std;

int main()
{
    bool hasChosen = false;
    string choice;

    while (hasChosen == false)
    {
        cout << "Would you like to run frame 1(1) or frame 2 (2)? ";
        cin >> choice;
        if (choice.compare("1") || choice.compare("2"))
        {
            hasChosen = true;
        }
        else
        {
            cout << endl << "You did not make a valid selection. Please try again. " <<endl;
        }
    }
    if (choice == "1")
    {
        frame1();
    }
    else
    {
        frame2();
    }
}