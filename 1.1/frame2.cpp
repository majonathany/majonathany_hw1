//
// Created by majonathany on 1/7/17.
//

#include "frame2.h"

#include <iostream>
#include <string>
using std::cin;
using std::cout;
using std::endl;
using std::string;

int frame2()
{
    cout << "What's your name? ";
    string name;
    cin >> name;

    // The message we intende to write
    const string greeting = "Hello, " + name + "!";
    const string mixed = "* " + string(greeting.size(), ' ') + " *";
    const string stars = string(mixed.size(), '*');

    cout << endl;
    cout << stars << endl
         << mixed << endl;
    cout << "* " << greeting << " *" << endl;
    cout << mixed << endl
         << stars << endl;

    return 0;
}

