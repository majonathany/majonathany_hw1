Jonathan Ma/HW#1/C++ For Advanced Programmers

Hi,

The answers to each problem is located in each folder. Header files were created automatically by CLion,
but there is not much content in them.

I used CMake (not sure how it works) in CLion to compile these files. The CMake file is found in CMakeLists.txt.

1.1) I create a tester.cpp file to test both frame1 and frame2. The program prompts you to choose which to run. I've
also attached an image of the compilation.

1.2) Same as 1 - running optimized_copy prints results and an image is attached.

1.3) Works correctly for less than 13 rows. Above 13 rows, incorrect values are calculated, and above 40 rows, the
program halts with a SIFGPE error. Perhaps this is a stack overflow.

1.4) CantDoThis.cpp will not compile, but the CanDoThis.c program will compile. The difference is that
you cannot write a variable called class in C++.

1.5) Answer is present in folder.